<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PhpImap\Exceptions\ConnectionException;
use PhpImap\Mailbox;
use App\Email;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $emails = Email::all();
        //print_r($emails);
        echo json_encode($emails);
    }

    public function store()
    {
        $emailServer = env('IMAP_HOSTNAME');
        $emailUser = env('IMAP_USERNAME');
        $userPassword = env('IMAP_PASSWORD');
        $mailData = array();
        $mailbox = new Mailbox(
            $emailServer, // IMAP server and mailbox folder
            $emailUser, // Username for the before configured mailbox
            $userPassword, // Password for the before configured username
                'UTF-8' // Server encoding (optional)
        );
        try {
        // Get all emails (messages)
            $mailsIds = $mailbox->searchMailbox('ALL');
        } catch(PhpImap\Exceptions\ConnectionException $ex) {
            echo json_encode($ex);
            die();
        }

        if(isset($mailsIds) && !empty($mailsIds))
        {
            foreach ($mailsIds as $singleMailId) {
                $email = $mailbox->getMail(
                $singleMailId, // ID of the email, you want to get
                false // Do NOT mark emails as seen (optional)
                );

                // Save mails to DB
                $fromName = (isset($email->fromName) ? $email->fromName : $email->fromAddress);
                $emilString = new Email;
                $emilString->id = $email->id;
                $emilString->fromName = $fromName;
                $emilString->fromAddress = $email->fromAddress;
                $emilString->toAddress = $email->toString;
                $emilString->subject = $email->subject;
                $emilString->message = utf8_encode($email->textHtml);
                $emilString->recevedOn = $email->date;
                $emilString->save();
                
                
            }

        }
        // If $mailsIds is empty, no emails could be fouemailsnd
        if(!$mailsIds) {
            die('Mailbox is empty');
        }
        echo json_encode($emails);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
