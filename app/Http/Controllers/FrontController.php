<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe;
use Image;
use App\Email;

class FrontController extends Controller
{
	public function home(){
        $emails = Email::all();
        return view('welcome',  array('emails'=>$emails));
    }

}
